#!/bin/bash
mkdir -p /srv/airtime/stor
chown -R www-data:www-data /srv/airtime
chown -R www-data:www-data /etc/airtime
chown -R postgres:postgres /var/lib/postgresql

echo "Restarting postgres DB"
pg_ctlcluster 10 main restart

if [ -e /liquidsoap ]
then
  echo "Setting up liquidsoap dir"
  mv /liquidsoap/* /usr/local/lib/python2.7/dist-packages/airtime_playout-1.0-py2.7.egg/liquidsoap/
  rm -rf /liquidsoap
fi

if [[ -v ${USE_SSL+x} ]]
then 
  if [[ -n ${LIBRETIME_DOMAIN+x} ]]
  then 
    echo "Creating ssl certificate for domain $LIBRETIME_DOMAIN"
    openssl req -x509 -out /etc/ssl/certs/fullchain.pem -keyout /etc/ssl/private/privkey.pem \
    -newkey rsa:2048 -nodes -sha256 -subj "/CN=${LIBRETIME_DOMAIN}" -extensions EXT -config <( \
    printf "[dn]\nCN=${LIBRETIME_DOMAIN}\n[req]\ndistinguished_name = dn\n[EXT]\nsubjectAltName=DNS:${LIBRETIME_DOMAIN}\nkeyUsage=digitalSignature\nextendedKeyUsage=serverAuth")
  fi 
  echo "Activating SSL config"
  a2ensite airtime-ssl
fi 

echo "Restarting libretime services"
/libre_start.sh

echo "Starting libretime container..."

### NECESSARY LIQUIDSOAP CHANGES
chown root:www-data /usr/bin/systemctl
chmod 770 /usr/bin/systemctl 
chown root:www-data /var/run/airtime-liquidsoap.service.status /var/log/journal/airtime-liquidsoap.service.log
chmod 770 /var/run/airtime-liquidsoap.service.status /var/log/journal/airtime-liquidsoap.service.log

